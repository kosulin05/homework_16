#include <iostream>
#include <time.h>

int main()
{
    struct tm buf;
    time_t t = time(NULL);
    const int size = 5;
    int sum = 0;
    localtime_s(&buf, &t);
    int day = buf.tm_mday;   
    int array[size][size]; 

    for (int i = 0; i < size; i++)
    {
        for (int j = 0; j < size; j++)
        {
            array[i][j] = i + j;
            std::cout << array[i][j] << " ";
            
        }
        std::cout << "\n";
    }
    std::cout << "\n";
    for (int j = 0; j < size; j++)
    {
        sum += array[day % size][j];
    }
    std::cout << "Today:" << day << "\n";
    std::cout << "Selected line:" << day % size << "\n";
    std::cout << "Line amount:" << sum;
}
